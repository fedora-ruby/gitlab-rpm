# Generated from omniauth-oauth-1.0.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name omniauth-oauth

Name: rubygem-%{gem_name}
Version: 1.0.1
Release: 1%{?dist}
Summary: A generic OAuth (1.0/1.0a) strategy for OmniAuth
Group: Development/Languages
License:
URL: https://github.com/intridea/omniauth-oauth
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(omniauth) => 1.0
Requires: rubygem(omniauth) < 2
Requires: rubygem(oauth)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(rspec) => 2.6
# BuildRequires: rubygem(rspec) < 3
# BuildRequires: rubygem(webmock)
# BuildRequires: rubygem(simplecov)
# BuildRequires: rubygem(rack-test)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
A generic OAuth (1.0/1.0a) strategy for OmniAuth.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 1.0.1-1
- Initial package
