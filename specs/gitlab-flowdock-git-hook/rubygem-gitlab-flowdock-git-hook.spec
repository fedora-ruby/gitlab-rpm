# Generated from gitlab-flowdock-git-hook-0.4.2.2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name gitlab-flowdock-git-hook

Name: rubygem-%{gem_name}
Version: 0.4.2.2
Release: 1%{?dist}
Summary: Git Post-Receive hook for Flowdock. Gem requirements patched for use with Gitlab
Group: Development/Languages
License: MIT
URL: http://github.com/bladealslayer/flowdock-git-hook
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(gitlab-grit) >= 2.4.1
Requires: rubygem(multi_json)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(rspec) => 2.8
# BuildRequires: rubygem(rspec) < 3
# BuildRequires: rubygem(jeweler) => 1.8.7
# BuildRequires: rubygem(jeweler) < 1.9
# BuildRequires: rubygem(webmock) >= 1.6.4
# BuildRequires: rubygem(jruby-openssl)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description



%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/LICENSE.txt
%doc %{gem_instdir}/README.md

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.4.2.2-1
- Initial package
