# Generated from gitlab_git-5.8.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name gitlab_git

Name: rubygem-%{gem_name}
Version: 5.8.0
Release: 1%{?dist}
Summary: Gitlab::Git library
Group: Development/Languages
License: MIT
URL: http://rubygems.org/gems/gitlab_git
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(gitlab-linguist) => 3.0
Requires: rubygem(gitlab-linguist) < 4
Requires: rubygem(gitlab-grit) => 2.6
Requires: rubygem(gitlab-grit) < 3
Requires: rubygem(activesupport) => 4.0
Requires: rubygem(activesupport) < 5
Requires: rubygem(rugged) => 0.19.0
Requires: rubygem(rugged) < 0.20
Requires: rubygem(charlock_holmes) => 0.6
Requires: rubygem(charlock_holmes) < 1
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
GitLab wrapper around git objects.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 5.8.0-1
- Initial package
