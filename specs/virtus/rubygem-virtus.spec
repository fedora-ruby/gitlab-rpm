# Generated from virtus-1.0.2.gem by gem2rpm -*- rpm-spec -*-
%global gem_name virtus

Name: rubygem-%{gem_name}
Version: 1.0.2
Release: 1%{?dist}
Summary: Attributes on Steroids for Plain Old Ruby Objects
Group: Development/Languages
License: MIT
URL: https://github.com/solnic/virtus
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(descendants_tracker) => 0.0.3
Requires: rubygem(descendants_tracker) < 0.1
Requires: rubygem(equalizer) => 0.0.9
Requires: rubygem(equalizer) < 0.1
Requires: rubygem(coercible) => 1.0
Requires: rubygem(coercible) < 2
Requires: rubygem(axiom-types) => 0.1
Requires: rubygem(axiom-types) < 1
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Attributes on Steroids for Plain Old Ruby Objects.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/LICENSE
%doc %{gem_instdir}/README.md
%doc %{gem_instdir}/TODO.md

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 1.0.2-1
- Initial package
