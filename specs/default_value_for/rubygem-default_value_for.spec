# Generated from default_value_for-3.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name default_value_for

Name: rubygem-%{gem_name}
Version: 3.0.0
Release: 1%{?dist}
Summary: Provides a way to specify default values for ActiveRecord models
Group: Development/Languages
License: 
URL: https://github.com/FooBarWidget/default_value_for
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(activerecord) >= 3.2.0
Requires: rubygem(activerecord) < 5.0
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby >= 1.9.3
# BuildRequires: rubygem(railties) >= 3.2.0
# BuildRequires: rubygem(railties) < 5.0
# BuildRequires: rubygem(minitest) >= 4.2
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
The default_value_for plugin allows one to define default values for
ActiveRecord models in a declarative manner.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 3.0.0-1
- Initial package
