# Generated from jquery-atwho-rails-0.4.9.gem by gem2rpm -*- rpm-spec -*-
%global gem_name jquery-atwho-rails

Name: rubygem-%{gem_name}
Version: 0.4.9
Release: 1%{?dist}
Summary: jquery plugin: @mentions
Group: Development/Languages
License:
URL: http://ichord.github.com/jquery-atwho-rails
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(rspec)
# BuildRequires: rubygem(generator_spec)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
This is a jQuery plugin
that implement Github-like mentions.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.4.9-1
- Initial package
