# Generated from gemnasium-gitlab-service-0.2.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name gemnasium-gitlab-service

Name: rubygem-%{gem_name}
Version: 0.2.1
Release: 1%{?dist}
Summary: Gemnasium service for Gitlab
Group: Development/Languages
License: MIT
URL: https://gemnasium.com/
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(rugged) => 0.19
Requires: rubygem(rugged) < 1
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
# BuildRequires: rubygem(rspec) => 2.14
# BuildRequires: rubygem(rspec) < 3
# BuildRequires: rubygem(webmock) => 1.17
# BuildRequires: rubygem(webmock) < 2
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Add Gemnasium support for Gitlab as a Project Service.
It uploads dependency files automatically on https://gemnasium.com API to
track your project dependencies.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.2.1-1
- Initial package
