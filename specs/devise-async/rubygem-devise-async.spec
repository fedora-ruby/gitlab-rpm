# Generated from devise-async-0.9.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name devise-async

Name: rubygem-%{gem_name}
Version: 0.9.0
Release: 1%{?dist}
Summary: Devise Async provides an easy way to configure Devise to send its emails asynchronously using your preferred queuing backend. It supports Resque, Sidekiq, Delayed::Job and QueueClassic
Group: Development/Languages
License: 
URL: https://github.com/mhfs/devise-async/
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems) 
Requires: rubygem(devise) => 3.2
Requires: rubygem(devise) < 4
BuildRequires: ruby(release)
BuildRequires: rubygems-devel 
BuildRequires: ruby 
# BuildRequires: rubygem(activerecord) >= 3.2
# BuildRequires: rubygem(actionpack) >= 3.2
# BuildRequires: rubygem(actionmailer) >= 3.2
# BuildRequires: rubygem(sqlite3) => 1.3
# BuildRequires: rubygem(sqlite3) < 2
# BuildRequires: rubygem(resque) => 1.20
# BuildRequires: rubygem(resque) < 2
# BuildRequires: rubygem(sidekiq) => 1.2
# BuildRequires: rubygem(sidekiq) < 2
# BuildRequires: rubygem(delayed_job_active_record) => 0.3
# BuildRequires: rubygem(delayed_job_active_record) < 1
# BuildRequires: rubygem(queue_classic) => 2.0
# BuildRequires: rubygem(queue_classic) < 3
# BuildRequires: rubygem(mocha) => 0.11
# BuildRequires: rubygem(mocha) < 1
# BuildRequires: rubygem(minitest) => 3.0
# BuildRequires: rubygem(minitest) < 4
# BuildRequires: rubygem(torquebox-no-op) => 2.3
# BuildRequires: rubygem(torquebox-no-op) < 3
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description
Send Devise's emails in background. Supports Resque, Sidekiq, Delayed::Job and
QueueClassic.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 0.9.0-1
- Initial package
