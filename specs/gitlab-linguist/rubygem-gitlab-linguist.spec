# Generated from gitlab-linguist-3.0.0.gem by gem2rpm -*- rpm-spec -*-
%global gem_name gitlab-linguist

Name: rubygem-%{gem_name}
Version: 3.0.0
Release: 1%{?dist}
Summary: GitHub Language detection
Group: Development/Languages
License: MIT
URL: https://github.com/github/linguist
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
Requires: rubygem(charlock_holmes) => 0.6.6
Requires: rubygem(charlock_holmes) < 0.7
Requires: rubygem(escape_utils) => 0.2.4
Requires: rubygem(escape_utils) < 0.3
Requires: rubygem(mime-types) => 1.19
Requires: rubygem(mime-types) < 2
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
# BuildRequires: rubygem(mocha)
# BuildRequires: rubygem(json)
# BuildRequires: rubygem(yajl-ruby)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

%description



%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


mkdir -p %{buildroot}%{_bindir}
cp -a .%{_bindir}/* \
        %{buildroot}%{_bindir}/

find %{buildroot}%{gem_instdir}/bin -type f | xargs chmod a+x

# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{_bindir}/linguist
%{gem_instdir}/bin
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 3.0.0-1
- Initial package
