# Generated from escape_utils-1.0.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name escape_utils

Name: rubygem-%{gem_name}
Version: 1.0.1
Release: 1%{?dist}
Summary: Faster string escaping routines for your web apps
Group: Development/Languages
License: MIT
URL: https://github.com/brianmario/escape_utils
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
Requires: ruby(release)
Requires: ruby(rubygems)
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby-devel >= 1.9.3
# BuildRequires: rubygem(rake-compiler) >= 0.7.5
# BuildRequires: rubygem(minitest) >= 5.0.0
# BuildRequires: rubygem(rack)
# BuildRequires: rubygem(haml)
# BuildRequires: rubygem(fast_xs)
# BuildRequires: rubygem(actionpack)
# BuildRequires: rubygem(url_escape)
Provides: rubygem(%{gem_name}) = %{version}

%description
Quickly perform HTML, URL, URI and Javascript escaping/unescaping.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{gem_extdir_mri}/lib
# TODO: move the extensions
mv %{buildroot}%{gem_instdir}/lib/shared_object.so %{buildroot}%{gem_extdir_mri}/lib/

# Prevent dangling symlink in -debuginfo (rhbz#878863).
rm -rf %{buildroot}%{gem_instdir}/ext/



# Run the test suite
%check
pushd .%{gem_instdir}

popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}

%changelog
* Mon Apr 21 2014 Achilleas Pipinellis <axilleas@fedoraproject.org> - 1.0.1-1
- Initial package
